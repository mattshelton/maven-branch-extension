package com.nuance.clu.maven;

import java.io.File;
import java.lang.reflect.Field;
import java.util.HashMap;
import java.util.Map;
import java.util.Properties;

import org.eclipse.aether.AbstractRepositoryListener;
import org.eclipse.aether.RepositoryEvent;
import org.eclipse.aether.RepositorySystemSession;
import org.eclipse.aether.metadata.Metadata;
import org.eclipse.aether.metadata.Metadata.Nature;
import org.eclipse.aether.repository.RemoteRepository;
import org.eclipse.aether.resolution.MetadataRequest;

/**
 * Manipulates an artifact's metadata in the local repository so that maven
 * fetches from the feature repository, and never the normal snapshot
 * repository, when on a feature branch. It also makes sure it doesn't use the
 * cached version of an artifact from a different feature branch.
 * 
 * @author alan_yackel
 *
 */
public class MetadataManipulatorRepositoryListener extends AbstractRepositoryListener {

	private static final String FEATURE_BRANCH_PROP = "feature.branch";
	private static final String TOUCHFILE_NAME = "resolver-status.properties";

	private GitFeatureInfo gitFeatureInfo;
	private TrackingFileManager trackingFileManager;
	private MetadataTracker metadataTracker;

	public MetadataManipulatorRepositoryListener(GitFeatureInfo gitFeatureInfo) {
		this.gitFeatureInfo = gitFeatureInfo;
		trackingFileManager = new TrackingFileManager();
		metadataTracker = new MetadataTracker();
	}

	@Override
	public void metadataResolving(RepositoryEvent event) {
		Metadata metadata = event.getMetadata();
		metadataTracker.resetIfNewArtifact(metadata);

		if (isEventForFeatureRepository(event)) {
			// Check the touch file and see if the last feature download was for
			// the current feature
			// branch. If not, delete the feature metadata file.
			RepositorySystemSession session = event.getSession();
			File metadataFile = new File(session.getLocalRepository().getBasedir(),
					session.getLocalRepositoryManager().getPathForRemoteMetadata(metadata,
							(RemoteRepository) event.getRepository(),
							((MetadataRequest) event.getTrace().getData()).getRequestContext()));

			File touchFile = getTouchFile(metadata, metadataFile);
			Properties props = readTouchFile(touchFile);
			String lastFeatureBranch = props.getProperty(FEATURE_BRANCH_PROP);
			if (lastFeatureBranch != null && !gitFeatureInfo.getCurrentGitBranch().equals(lastFeatureBranch)) {
				// Remove metadata file because it isn't for this branch
				if (metadataFile != null && metadataFile.exists()) {
					metadataFile.delete();
				}
				// Clear FEATURE_BRANCH_PROP
				Map<String, String> updateMap = new HashMap<String, String>();
				updateMap.put(FEATURE_BRANCH_PROP, null);
				trackingFileManager.update(touchFile, updateMap);
			}
		}
	}

	@Override
	public void metadataDownloaded(RepositoryEvent event) {
		if (isEventForFeatureRepository(event)) {
			metadataTracker.setFeatureMetadataFound(event.getException() == null);
			modifyTrackingReferencesToFeatureRepo(event);
		}
	}

	private void modifyTrackingReferencesToFeatureRepo(RepositoryEvent event) {
		Metadata metadata = event.getMetadata();
		File metadataFile = event.getFile();

		File touchFile = getTouchFile(metadata, metadataFile);
		Properties props = readTouchFile(touchFile);
		Map<String, String> updateMap = new HashMap<String, String>();
		// Remove feature repo references
		for (final String name : props.stringPropertyNames()) {
			String value = props.getProperty(name);
			if (name.contains(gitFeatureInfo.getFeatureRepositoryName())) {
				value = null;
				updateMap.put(name, value);
			}
		}
		// Track feature branch if found in repo
		if (event.getException() == null && isOnFeatureBranch()) {
			updateMap.put(FEATURE_BRANCH_PROP, gitFeatureInfo.getCurrentGitBranch());
		}
		trackingFileManager.update(touchFile, updateMap);
	}

	private File getTouchFile(Metadata metadata, File metadataFile) {
		return new File(metadataFile.getParent(), TOUCHFILE_NAME);
	}

	private Properties readTouchFile(File touchFile) {
		Properties props = trackingFileManager.read(touchFile);
		return (props != null) ? props : new Properties();
	}

	@Override
	public void metadataResolved(RepositoryEvent event) {
		Metadata metadata = event.getMetadata();
		File metadataFile = metadata.getFile();

		if (metadata.getNature() == Nature.SNAPSHOT && metadataFile != null && isOnFeatureBranch()) {
			if (isEventForFeatureRepository(event)) {
				resolveFeatureSnapshot(metadata);
			} else {
				resolveSnapshot(metadata);
			}
		}
	}

	private void resolveFeatureSnapshot(Metadata metadata) {
		metadataTracker.setFeatureMetadata(metadata);
		if (metadataTracker.isSnapshopMetadataFound()) {
			metadataTracker.clearSnapshotFileFromMetadata();
		}
	}

	private void resolveSnapshot(Metadata metadata) {
		metadataTracker.setSnapshopMetadata(metadata);
		if (metadataTracker.isFeatureMetadataFound()) {
			metadataTracker.clearSnapshotFileFromMetadata();
		}
	}

	private boolean isOnFeatureBranch() {
		return gitFeatureInfo.getCurrentGitBranch().startsWith(gitFeatureInfo.getFeaturePrefix());
	}

	private boolean isEventForFeatureRepository(RepositoryEvent event) {
		return event.getRepository().getId().equals(gitFeatureInfo.getFeatureRepositoryName());
	}

	/**
	 * Tracks metadata for the current artifact metadata being downloaded.
	 *
	 */
	class MetadataTracker {

		private String artifactId;
		private Boolean metadataFound;
		private Metadata snapshopMetadata;
		private Metadata featureMetadata;

		public void resetIfNewArtifact(Metadata metadata) {
			String newArtifactId = metadata.getArtifactId();
			if (!newArtifactId.equals(artifactId)) {
				artifactId = newArtifactId;
				metadataFound = null;
				snapshopMetadata = null;
			}
		}

		public void setFeatureMetadataFound(boolean found) {
			metadataFound = found;
		}

		public boolean isFeatureMetadataFound() {
			return metadataFound;
		}

		public boolean isSnapshopMetadataFound() {
			return snapshopMetadata != null;
		}

		public void setSnapshopMetadata(Metadata snapshopMetadata) {
			this.snapshopMetadata = snapshopMetadata;
		}

		public void setFeatureMetadata(Metadata featureSnapshopMetadata) {
			featureMetadata = featureSnapshopMetadata;
			setFeatureMetadataFound(true);
		}

		public void clearFeatureFileFromMetadata() {
			clearMetadataFileField(featureMetadata);
		}

		public void clearSnapshotFileFromMetadata() {
			clearMetadataFileField(snapshopMetadata);
		}

		private void clearMetadataFileField(Metadata metadata) {
			try {
				Field fileField = null;
				fileField = metadata.getClass().getDeclaredField("file");
				fileField.setAccessible(true);
				fileField.set(metadata, null);
			} catch (NoSuchFieldException | SecurityException | IllegalArgumentException | IllegalAccessException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
	}

}
