package com.nuance.clu.maven;

import java.io.IOException;
import java.util.List;
import java.util.Properties;

import org.apache.maven.AbstractMavenLifecycleParticipant;
import org.apache.maven.MavenExecutionException;
import org.apache.maven.execution.MavenExecutionRequest;
import org.apache.maven.execution.MavenSession;
import org.apache.maven.model.Plugin;
import org.apache.maven.model.Profile;
import org.apache.maven.plugin.MojoExecution;
import org.apache.maven.plugin.PluginParameterExpressionEvaluator;
import org.apache.maven.project.MavenProject;
import org.codehaus.plexus.PlexusContainer;
import org.codehaus.plexus.component.annotations.Component;
import org.codehaus.plexus.component.annotations.Requirement;
import org.codehaus.plexus.component.configurator.expression.ExpressionEvaluationException;
import org.codehaus.plexus.logging.Logger;
import org.eclipse.aether.DefaultRepositorySystemSession;
import org.eclipse.aether.RepositoryListener;
import org.eclipse.aether.util.listener.ChainedRepositoryListener;

/**
 * Maven extension that will set a property with the name of the current git
 * branch in all of the projects in the reactor, and manage download of
 * artifacts so that they come from a feature repository when on a feature
 * branch.
 *
 * @author alan_yackel
 */
@Component(role = AbstractMavenLifecycleParticipant.class)
public class FeatureBranchExtension extends AbstractMavenLifecycleParticipant implements GitFeatureInfo {

	private static final String BRANCH_PROPERTY_PROPERTY = "features.branchProperty";
	private static final String FEATURE_REPOSITORY_NAME = "features.repositoryName";
	private static final String DOT_GIT_DIRECTORY_PROPERTY = "feature.dotGitDirectory";
	private static final String FEATURE_PREFIX = "feature";

	@Requirement
	private Logger logger;

	@Requirement
	private PlexusContainer container;

	private String gitDirectoryLocation = "${project.basedir}/.git";
	private String featureRepositoryName = null;
	private String branchProperty = null;
	private PluginParameterExpressionEvaluator evaluator;
	private String currentGitBranch;

	@Override
	public void afterSessionStart(MavenSession session) throws MavenExecutionException {
		handleFeatureProperties(session);
		addRepositoryMetadataListener(session);
	}

	/**
	 * Look up the names for feature related properties
	 * 
	 * @param session
	 */
	private void handleFeatureProperties(MavenSession session) {
		MavenExecutionRequest request = session.getRequest();
		List<String> activeProfileNames = request.getActiveProfiles();
		for (String activeProfileName : activeProfileNames) {
			List<Profile> profiles = request.getProfiles();
			for (Profile profile : profiles) {
				if (profile.getId().equals(activeProfileName)) {
					Properties properties = profile.getProperties();
					featureRepositoryName = findPropertyIfExists(properties, FEATURE_REPOSITORY_NAME,
							featureRepositoryName);
					branchProperty = findPropertyIfExists(properties, BRANCH_PROPERTY_PROPERTY, branchProperty);
					gitDirectoryLocation = findPropertyIfExists(properties, DOT_GIT_DIRECTORY_PROPERTY,
							gitDirectoryLocation);
				}
			}
		}
	}

	private String findPropertyIfExists(Properties properties, String propertyName, String field) {
		String prop = properties.getProperty(propertyName);
		return prop != null ? prop : field;
	}

	private void addRepositoryMetadataListener(MavenSession session) throws MavenExecutionException {
		DefaultRepositorySystemSession repositorySession = (DefaultRepositorySystemSession) session
				.getRepositorySession();

		RepositoryListener currentRepositoryListener = repositorySession.getRepositoryListener();
		MetadataManipulatorRepositoryListener metadataManipulatorRepositoryListener = new MetadataManipulatorRepositoryListener(
				this);
		ChainedRepositoryListener repositoryListener = new ChainedRepositoryListener(currentRepositoryListener,
				metadataManipulatorRepositoryListener);
		// repositoryListener.add(new ConsoleRepositoryListener());
		repositorySession.setRepositoryListener(repositoryListener);
	}

	@Override
	public void afterProjectsRead(MavenSession session) throws MavenExecutionException {
		evaluator = createParameterEvaluator(session);
		setupGitBranchProperty(session);
		fixUrlsForRepositories(session);
	}

	private PluginParameterExpressionEvaluator createParameterEvaluator(MavenSession session) {
		Plugin plugin = session.getTopLevelProject().getPlugin("com.nuance.clu.maven:maven-branch-extension");
		MojoExecution mojoExecution = new MojoExecution(plugin, null, null);
		PluginParameterExpressionEvaluator evaluator = new PluginParameterExpressionEvaluator(session, mojoExecution);
		return evaluator;
	}

	private void setupGitBranchProperty(MavenSession session) throws MavenExecutionException {
		try {
			gitDirectoryLocation = (String) evaluator.evaluate(gitDirectoryLocation);
		} catch (ExpressionEvaluationException e) {
			logger.error("Unable to evaluate expression for git branch", e);
			throw new MavenExecutionException("Unable to evaluate Git branch expression", e);
		}
		currentGitBranch = getGitBranch(session.getCurrentProject());

		Properties systemProperties = session.getUserProperties();
		systemProperties.setProperty(branchProperty, currentGitBranch);

		// Recreate evaluator because we've added a property
		evaluator = createParameterEvaluator(session);
	}

	private String getGitBranch(MavenProject mavenProject) throws MavenExecutionException {
		GitProvider gitProvider = new JGitProvider(gitDirectoryLocation, mavenProject);
		gitProvider.init();
		try {
			return gitProvider.getBranchName();
		} catch (IOException e) {
			throw new MavenExecutionException(e.getMessage(), e);
		}
	}

	private void fixUrlsForRepositories(MavenSession session) throws MavenExecutionException {
		RepositoryUrlRewriter repositoryUrlRewriter = new RepositoryUrlRewriter(evaluator, logger);
		for (MavenProject mavenProject : session.getAllProjects()) {
			repositoryUrlRewriter.fixProjectsRepositories(mavenProject);
		}
	}

	@Override
	public String getFeatureRepositoryName() {
		return featureRepositoryName;
	}

	@Override
	public String getCurrentGitBranch() {
		return currentGitBranch;
	}

	@Override
	public String getFeaturePrefix() {
		return FEATURE_PREFIX;
	}
}
