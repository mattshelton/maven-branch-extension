package com.nuance.clu.maven;

import java.util.List;

import org.apache.maven.MavenExecutionException;
import org.apache.maven.RepositoryUtils;
import org.apache.maven.artifact.repository.ArtifactRepository;
import org.apache.maven.model.Repository;
import org.apache.maven.plugin.PluginParameterExpressionEvaluator;
import org.apache.maven.project.MavenProject;
import org.codehaus.plexus.component.configurator.expression.ExpressionEvaluationException;
import org.codehaus.plexus.logging.Logger;
import org.eclipse.aether.repository.RemoteRepository;

/**
 * Modifies or rewrites the URLs for the various repositories related to a maven
 * project by evaluating expressions and setting the new evaluated URL string.
 * 
 * @author alan_yackel
 *
 */
public class RepositoryUrlRewriter {

	private Logger logger;

	/**
	 * Evaluator for plugin parameters expressions
	 */
	private PluginParameterExpressionEvaluator evaluator;

	public RepositoryUrlRewriter(PluginParameterExpressionEvaluator evaluator, Logger logger) {
		this.logger = logger;
		this.evaluator = evaluator;
	}

	public void fixProjectsRepositories(MavenProject mavenProject) throws MavenExecutionException {
		try {
			fixRepositories(mavenProject);
			fixRemoteRepositories(mavenProject);
			fixDeploymentArtifactRepository(mavenProject);
		} catch (ExpressionEvaluationException e) {
			logger.error("Unable to evaluate expression", e);
			throw new MavenExecutionException("Unable to evaluate expression", e);
		}

	}

	private void fixRepositories(MavenProject mavenProject) throws ExpressionEvaluationException {
		List<Repository> repositories = mavenProject.getRepositories();
		for (Repository repository : repositories) {
			fixRepository(repository);
		}
	}

	private void fixRepository(Repository repository) throws ExpressionEvaluationException {
		logger.debug(
				"Repo: id " + repository.getId() + ", name " + repository.getName() + ", url " + repository.getUrl());
		String evalUrl = (String) evaluator.evaluate(repository.getUrl());
		logger.debug(evalUrl);
		repository.setUrl(evalUrl);
	}

	private void fixRemoteRepositories(MavenProject mavenProject) throws ExpressionEvaluationException {
		List<ArtifactRepository> remoteArtifactRepositories = mavenProject.getRemoteArtifactRepositories();
		for (ArtifactRepository remoteArtifactRepository : remoteArtifactRepositories) {
			fixArtifactRepository("Remote artifact", remoteArtifactRepository);
		}

		// Since we can't modify the remote repos we replace them all
		List<RemoteRepository> remoteProjectRepositories = mavenProject.getRemoteProjectRepositories();
		remoteProjectRepositories.clear();
		for (ArtifactRepository remoteArtifactRepository : remoteArtifactRepositories) {
			remoteProjectRepositories.add(RepositoryUtils.toRepo(remoteArtifactRepository));
			logger.debug("Remote artifact repo: id " + remoteArtifactRepository.getId() + ", key "
					+ remoteArtifactRepository.getKey() + ", url " + remoteArtifactRepository.getUrl());
		}
	}

	private void fixDeploymentArtifactRepository(MavenProject mavenProject) throws ExpressionEvaluationException {
		ArtifactRepository artifactRepository = mavenProject.getDistributionManagementArtifactRepository();
		fixArtifactRepository("Deployment snapshot artifact", artifactRepository);
	}

	private void fixArtifactRepository(String repoType, ArtifactRepository artifactRepository)
			throws ExpressionEvaluationException {
		if(artifactRepository == null)
			return;
		
		logger.debug(repoType + " repo: id " + artifactRepository.getId() + ", key " + artifactRepository.getKey()
				+ ", url " + artifactRepository.getUrl());
		String evalUrl = (String) evaluator.evaluate(artifactRepository.getUrl());
		artifactRepository.setUrl(evalUrl);
		logger.debug(evalUrl);
	}

}
